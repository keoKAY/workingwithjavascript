// function sayHello(message = "Subject") { // message is a param
//     console.log("=================================")
//     console.log("      Welcome to " + message)
//     console.log("=================================")
//     return message;
// }
const sayHello = function (message = "subject") {
    console.log("=================================")
    console.log("      Welcome to " + message)
    console.log("=================================")
    return message;
}

// arrow functions 
const sayHelloArrow = (message = "subject") => {
    console.log("=================================")
    console.log("      Welcome to " + message)
    console.log("=================================")
    return message;

}

function addNumbers(a = 0, b = 0) {
    return a + b;
}
const addNumberArrow = (a = 0, b = 0) => a + b;
// call a function 
// javascript , css (argument)
// sayHello("Javascript")
let subject = sayHelloArrow("CSS")
console.log(`${subject} it is very interesting!`)
// this is for one time use ( for subject variable)
console.log(`${sayHello("CSS")} it is very interesting!`)

