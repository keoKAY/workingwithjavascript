let scores = [23, 45, 78, 34]
let names = new Array("piseth", "pisey", "james")


scores[0] = 32; // update 
scores.push(100) // add new element to the array
scores.splice(2, 0, "I'm here") // add element to specific location
console.log("Deleted Score : ", scores.shift())
// pop() will remove the last item
console.log("Deleted Score is : ", scores.pop())


names.push("Bona", "June")
names.unshift("tena") // add element to the first
console.log("Deleted Name is : ", names.pop())


console.log("All Elements in these arrays : ")
console.log("Scores : ", scores)

// 1 : start index 
// 4 : number of items to be delete
let deleteItems = names.splice(1, 4)
console.log("Deleted Items : ", deleteItems)
console.log("Names : ", names)

let allArrays = scores.concat(names)
console.log("All Arrays:", allArrays)
let mergedArrays = [...scores, ...names] // ... spread operator
console.log("Merged Arrays are :",mergedArrays)