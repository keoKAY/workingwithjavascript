
// for loop , initial , conditrion , update 
// var i=0 ; 
// for(; i < 10; ){
//     console.log("hello world ")
//    // i++; -> will cause an infinite loop as well  
// }

// // this is called an infinite loop 
// for(;;){
//     console.log("This loop runs forever ! ")
// }


var j = 0;
while (j < 10) {
    if (j == 5) {
        break;
    }
    console.log(j + ".programming is fun!!! ")
    j++
}

for (var i = 0; i < 10; i++) {
    if (i == 5) continue;
    console.log("Value of i = ", i)
}

// while(true){
//     console.log("This will also run forever!")
//     break; // will terminate the loop 
// }

console.log("This is the dowhilie loop here ! ")
var z = 100;
do {
    console.log(z+".Hello Worldddd ! ")
    z++;
} while (z < 10); 
//  for loop , while , do-while 