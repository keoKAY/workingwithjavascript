let personList = [

    {
        "username": "James Doe",
        "gender": "male",
        "email": "james123@gmail.com",
        "address": "Phnom Penh"
    },
    {
        "username": "Bona Tina",
        "gender": "female",
        "email": "tina123@gmail.com",
        "address": "Siem Reap"
    },
    {
        "username": "Piseth Mony",
        "gender": "male",
        "email": "monypiseth@gmail.com",
        "address": "Phnom Penh"
    },
    {
        "username": "Jane Junla",
        "gender": "female",
        "email": "junla445@gmail.com",
        "address": "Battambang"
    },
    {
        "username": "Jariya Sambath",
        "gender": "female",
        "email": "jariyasambath@gmail.com",
        "address": "Phnom Penh"
    },

]

// array.filter()! 
const findPersonByGender = (gender = "male") =>
    personList.filter(person => person.gender == gender)

//  find only female 
let femaleList = findPersonByGender("female")
console.log("Female List are : ")
console.log(femaleList)
// find only male 
let maleList = findPersonByGender("male")
console.log("Male List are : ")
console.log(maleList)

// forEach
console.log("Printing all person using forEach : ")
let filteredAddress = "siem reap"
personList.forEach(person => {
    if (person.address.toLocaleLowerCase() === filteredAddress.toLocaleLowerCase()) {
        console.log(person)
    }
})


// use forEach with either map or filter 
console.log("Using chaining method filter().forEach()")
personList
    .filter(
        person => person.address.toLowerCase() === filteredAddress.toLowerCase()
    )
    .forEach(
        person => console.log(person)
    )

// using map 
let scores = [12, 45, 30, 67, 89]
let updatedScores = scores.map(score => score + 10)
console.log("original scores : ")
console.log(scores)
console.log("updated scores : ")
console.log(updatedScores)