
// # for loop 
// initial , condition , update
for (var i = 1; i <= 10; i++) {
    // print only even 
    if (i % 2 != 0) {
        console.log(i + ".Hello World ")
    }

}

// print only when i is even 
console.log("Show only even values : ")
for (var i = 2; i <= 10; i += 2) {
    console.log(i + ".Hello World ")
}



// n = 60
// 1,2,3,4,5,6,7,8,....n 
// find sum 

var sum = 0;
var n = 3;
for (var i = 1; i <= n; i++) {
    // console.log(i)
    sum = sum + i;
    //sum+=i; 
    // i = 1, sum = 0 + 1 
    // i = 2 , sum = 1 + 2  
    // i = 3 , sum = 3 + 3
}
console.log(`Total addition of ${n} start from 1 : ${sum}`)

// n = ?
// 1 , 3, 5, 7, 9,........
var n1 = 5;
var sum1 = 0;
for (var i = 1; i <= n1; i++) {
    if (i % 2 == 0)
        sum1 += i;

}
// 1 , 2 , 3, 4, 5 
console.log(`Total value of EVEN Numbers start from 1 to ${n1} = ${sum1}`)
// 2 , 4, 6, 8, 10 , .......
var n2 = 5;
var sum2 = 0;
for (var i = 1; i <= n2; i++) {
    if (i % 2 != 0)
        sum2 += i;
}
console.log(`Total value of ODD Numbers start from 1 to ${n2} = ${sum2}`)