let foods = [
    {
        "title": "Pizza",
        "description": "delicious pizza for $5",
        "imageUrl": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTNu1KQKRAPGul3Uq2s45Pz0K922aDV0UnB9I95OWJjTg&s"
    },
    {
        "title": "Burger",
        "description": "delicious Burger for $5",
        "imageUrl": "https://cdn3d.iconscout.com/3d/premium/thumb/burger-4065243-3361782.png"
    },
    {
        "title": "Chicken Wings",
        "description": "delicious chicken wings for $5",
        "imageUrl": "https://cdn3d.iconscout.com/3d/premium/thumb/chicken-wing-10547854-8488603.png"
    },
]
let foodSelect = document.getElementById("foodSelect")
let cardTitle = document.getElementById("cardTitle")
let cardDescription = document.getElementById("cardDescription")
let cardImage = document.getElementById("cardImage")
const handleSelect = function (event) {
    let userOption = event.target.value; // get the value from select 
    let chosenIndex = 0;
    switch (userOption) {
        case "pizza":
            chosenIndex = 0;
            break;
        case "burger":
            chosenIndex = 1;
            break;
        case "chicken":
            chosenIndex = 2;
            break;
        default:
            alert("Invalid Option ! ")
            break;
    }

    cardTitle.innerText = foods[chosenIndex].title
    cardDescription.innerText = foods[chosenIndex].description
    cardImage.src = foods[chosenIndex].imageUrl
}
foodSelect.addEventListener('change', handleSelect)



// this code the the grade checkers

let scoreInput = document.getElementById("scoreInput")
let gradeOutput = document.getElementById("gradeOutput")
let checkResultButton = document.getElementById("checkResultBtn")

gradeOutput.innerText = "_"
const handleInput = function (e) {
    console.log(e.target.value)
    let score = parseInt(e.target.value);
    if (score < 0 || score > 100) {
        checkResultButton.disabled = true;
    } else {
        checkResultButton.disabled = false;
    }
}
scoreInput.addEventListener('input', handleInput)

function onCheckResultClick() {
    let scoreValue = scoreInput.value;
    let grade = "Invalid"
    if (scoreValue >= 95) {
        grade = "A"
    } else if (scoreValue >= 90) {
        grade = "B"
    } else if (scoreValue >= 85) {
        grade = "C"
    } else if (scoreValue >= 70) {
        grade = "D"
    } else if (scoreValue >= 50) {

        grade = "E"
    } else {
        gradeOutput.style.color = 'red'
        grade = "F"
    }
    gradeOutput.innerText = grade;

}